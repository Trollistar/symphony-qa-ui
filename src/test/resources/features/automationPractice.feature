Feature: Automation practice overall functionality

  Scenario: Navigation to the website
    When user navigates to automation practice website
    Then the logo is present

  Scenario: User registration
    Given user navigates to automation practice website
    And clicks on Sign in button
    And enters a random email
    And clicks on Create account button
    And populates the required fields with valid data
    When user clicks on the Register button
    Then the Sign out button is present

  Scenario Outline: User registering for invalid email
    Given user navigates to automation practice website
    And clicks on Sign in button
    When user enters an email "<email>"
    And clicks on Create account button
    Then an appropriate error "<error>" is displayed
    Examples:
      | email       | error                                                                                                                |
      | qwe@qwe.com | An account using this email address has already been registered. Please enter a valid password or request a new one. |
      | qweqwe.com  | Invalid email address.                                                                                               |

  Scenario Outline: Products by category
    Given user navigates to automation practice website
    When user clicks on category "<category>"
    Then 7 products should be present
    Examples:
      | category     |
      | Popular      |
      | Best Sellers |

  Scenario: Search for printed dresses
    Given user navigates to automation practice website
    And enters printed dresses in the search field
    And clicks on Search button
    When he saves the results
    Then the text files contains the correct results