package is.symphony.qa.stepdefs;

import com.github.javafaker.Faker;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import is.symphony.qa.browser.Browser;
import is.symphony.qa.data.AccountCreationData;
import is.symphony.qa.helpers.FileHelper;
import is.symphony.qa.pages.AutomationPractice;
import org.testng.Assert;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AutomationPracticeStepDefs {
    private AutomationPractice automationPractice;

    @Before
    public void setup() {
        automationPractice = new AutomationPractice();
    }

    @After
    public void cleanup() {
        Browser.disposeOfBrowser();
    }


    @Given("^user navigates to automation practice website$")
    public void userNavigatesToAutomationPracticeWebsite() {
        Browser.navigateToUrl("http://automationpractice.com/index.php");
    }

    @And("^clicks on Sign in button$")
    public void clicksOnSignInButton() {
        automationPractice.headerPage().signInButton().click();
    }

    @And("^clicks on Create account button$")
    public void clicksOnCreateAccountButton() {
        automationPractice.authenticationPage().createAnAccountButton().click();
    }

    @When("^user clicks on the Register button$")
    public void userClicksOnTheRegisterButton() {
        automationPractice.createAnAccountPage().registerButton().click();
    }

    @Then("^the Sign out button is present$")
    public void theSignOutButtonIsPresent() {
        Assert.assertNotNull(
                automationPractice.headerPage().signOutButton(),
                "Sign out button is missing after registering an account.");
    }

    @When("user clicks on category {string}")
    public void userClicksOnCategory(String category) {
        automationPractice.landingPage().category(category).click();
    }

    @Then("^(\\d+) products should be present$")
    public void productsShouldBePresent(int numberOfProducts) {
        Assert.assertEquals(automationPractice.searchResultsPage().availableProductImages().size(), numberOfProducts);
    }

    @And("^enters printed dresses in the search field$")
    public void entersPrintedDressesInTheSearchField() {
        automationPractice.landingPage().searchField().sendKeys("Printed dresses");
    }

    @And("^clicks on Search button$")
    public void clicksOnSearchButton() {
        automationPractice.landingPage().searchButton().click();
    }

    @When("^he saves the results$")
    public void heSavesTheResults() {
        automationPractice.searchResultsPage().productList();

        List<String> productNames = automationPractice.searchResultsPage().availableProductImages()
                .stream().map(x -> x.getAttribute("title")).collect(Collectors.toList());

        FileHelper.writeListToFile(productNames);
    }

    @Then("^the text files contains the correct results$")
    public void theTextFilesContainsTheCorrectResults() {
        List<String> readLines = FileHelper.readTextFromFile();

        Assert.assertEquals(Objects.requireNonNull(readLines).size(), 5);

        Assert.assertEquals(readLines.get(0), "Printed Summer Dress");
        Assert.assertEquals(readLines.get(1), "Printed Dress");
        Assert.assertEquals(readLines.get(2), "Printed Chiffon Dress");
        Assert.assertEquals(readLines.get(3), "Printed Summer Dress");
        Assert.assertEquals(readLines.get(4), "Printed Dress");
    }

    @And("enters a random email")
    public void entersARandomEmail() {
        Faker faker = new Faker();

        userEntersAnEmail(faker.bothify("?#?#?#?#?#?#?#?#?#?#@test.com"));
    }

    @And("populates the required fields with valid data")
    public void userPopulatesTheRequiredFieldsWithValidData() {
        AccountCreationData data = new AccountCreationData();

        automationPractice.createAnAccountPage().mrRadioButton().click();
        automationPractice.createAnAccountPage().firstNameField().sendKeys(data.getFirstName());
        automationPractice.createAnAccountPage().lastNameField().sendKeys(data.getLastName());
        automationPractice.createAnAccountPage().passwordField().sendKeys(data.getPassword());
        automationPractice.createAnAccountPage().dateOfBirthDayDropdown().selectFromDropdown(data.getDateOfBirthDay());
        automationPractice.createAnAccountPage().dateOfBirthMonthDropdown().selectFromDropdown(data.getDateOfBirthMonth());
        automationPractice.createAnAccountPage().dateOfBirthYearDropdown().selectFromDropdown(data.getDateOfBirthYear());
        automationPractice.createAnAccountPage().addressFirstNameField().sendKeys(data.getFirstName());
        automationPractice.createAnAccountPage().addressLastNameField().sendKeys(data.getLastName());
        automationPractice.createAnAccountPage().companyField().sendKeys(data.getCompany());
        automationPractice.createAnAccountPage().addressField().sendKeys(data.getAddress());
        automationPractice.createAnAccountPage().cityField().sendKeys(data.getCity());
        automationPractice.createAnAccountPage().stateDropdown().selectFromDropdown(data.getState());
        automationPractice.createAnAccountPage().zipCodeField().sendKeys(data.getZipCode());
        automationPractice.createAnAccountPage().countryDropdown().selectFromDropdown(data.getCountry());
        automationPractice.createAnAccountPage().additionalInformationField().sendKeys(data.getAdditionalInformation());
        automationPractice.createAnAccountPage().homePhoneField().sendKeys(data.getHomePhone());
        automationPractice.createAnAccountPage().mobilePhoneField().sendKeys(data.getMobilePhone());
        automationPractice.createAnAccountPage().addressAliasField().sendKeys(data.getAddressAlias());
    }

    @Then("the logo is present")
    public void theLogoIsPresent() {
        Assert.assertNotNull(automationPractice.landingPage().logo());
    }

    @When("user enters an email {string}")
    public void userEntersAnEmail(String email) {
        automationPractice.authenticationPage().newAccountEmailField().sendKeys(email);
    }

    @Then("an appropriate error {string} is displayed")
    public void anAppropriateErrorIsDisplayed(String error) {
        Assert.assertEquals(automationPractice.authenticationPage().accountCreationErrorMessage().getText(), error);
    }
}
