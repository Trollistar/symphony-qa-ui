package is.symphony.qa.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import is.symphony.qa.helpers.LoggingHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

class DriverFactory {
    private DriverFactory() {
    }

    /**
     * The entire stack of this simple factory's main method will:
     * 1) Verify the version of the desired browser
     * 2) Verify the version of the currently downloaded driver binary
     * 3) Download the binary for the desired browser's version if there is a mismatch
     * 4) Instantiate the appropriate WebDriver
     *
     * @param browserType Configurable browser type
     * @return New instance of the desired WebDriver, based on the application configuration
     */
    static WebDriver getWebDriver(String browserType) {

        switch (browserType.toLowerCase()) {
            case "chrome":
                return getChromeDriver();

            case "firefox":
                return getFirefoxDriver();

            case "ie":
                return getInternetExplorerDriver();

            default:
                LoggingHelper.log(String.format(
                        "No valid currentBrowser configuration found in app.properties file.%n" +
                                "The browserType set was: %s%n" +
                                "Defaulting back to ChromeDriver.", browserType));
                return getChromeDriver();
        }
    }

    private static ChromeDriver getChromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("disable-extensions", "no-sandbox");
        return new ChromeDriver(chromeOptions);
    }

    private static FirefoxDriver getFirefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

    private static InternetExplorerDriver getInternetExplorerDriver() {
        WebDriverManager.iedriver().setup();
        return new InternetExplorerDriver();
    }
}
