package is.symphony.qa.browser;

/**
 * Serves to encapsulate all the actions that have to do directly with the browser control.
 */
public class Browser {

    private Browser() {
    }

    /**
     * Used to tear down the currentBrowser and set its value to null
     * to allow new Webdriver instance to be created.
     */
    public static void disposeOfBrowser() {
        DriverManager.getDriver().quit();
        DriverManager.removeDriver();
    }

    /**
     * Used to navigate to a web page.
     *
     * @param url Url to navigate to
     */
    public static void navigateToUrl(String url) {
        DriverManager.getDriver().get(url);
    }
}
