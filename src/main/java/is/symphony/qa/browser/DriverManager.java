package is.symphony.qa.browser;

import is.symphony.qa.helpers.TestConfiguration;
import org.openqa.selenium.WebDriver;

public class DriverManager {
    private static ThreadLocal<WebDriver> drivers = new ThreadLocal<>();

    private DriverManager() {
    }

    /**
     * Retrieves the instance that is tied to the current Thread Id.
     *
     * @return WebDriver instance tied to the Thread id.
     */
    public static WebDriver getDriver() {
        if (drivers.get() == null) {
            setDriver();
        }

        return drivers.get();
    }

    /**
     * Creates a new instance of WebDriver based on the application configurations and
     * then stores it in the "drivers" collection with current Thread Id as a reference to it.
     */
    private static void setDriver() {
        drivers.set(DriverFactory.getWebDriver(TestConfiguration.getBrowserType()));
    }

    /**
     * Removes the WebDriver instance that is tied to the current Thread Id.
     */
    static void removeDriver() {
        drivers.remove();
    }
}
