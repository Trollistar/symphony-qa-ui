package is.symphony.qa.data;

public class AccountCreationData {
    private String firstName;
    private String lastName;
    private String password;
    private String dateOfBirthDay;
    private String dateOfBirthMonth;
    private String dateOfBirthYear;
    private String company;
    private String address;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private String country;
    private String additionalInformation;
    private String homePhone;
    private String mobilePhone;
    private String addressAlias;

    public AccountCreationData() {
        firstName = "Tester";
        lastName = "Testington";
        password = "Testing123!";
        dateOfBirthDay = "15  ";
        dateOfBirthMonth = "January ";
        dateOfBirthYear = "2001  ";
        company = "Testing Company";
        address = "Testing Road 1";
        addressLine2 = "Apartment Test";
        city = "Test City";
        state = "Alabama";
        zipCode = "35005";
        country = "United States";
        additionalInformation = "Additional information";
        homePhone = "123123";
        mobilePhone = "456456";
        addressAlias = "Tester address alias";
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getDateOfBirthDay() {
        return dateOfBirthDay;
    }

    public String getDateOfBirthMonth() {
        return dateOfBirthMonth;
    }

    public String getDateOfBirthYear() {
        return dateOfBirthYear;
    }

    public String getCompany() {
        return company;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountry() {
        return country;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getAddressAlias() {
        return addressAlias;
    }
}
