package is.symphony.qa.pages;

import org.openqa.selenium.WebElement;

public class AuthenticationPage extends BasePage {

    private static final String NEW_ACCOUNT_EMAIL_FIELD_ID = "email_create";
    private static final String CREATE_AN_ACCOUNT_BUTTON_NAME = "SubmitCreate";
    private static final String ACCOUNT_CREATION_ERROR_MESSAGE_ID = "create_account_error";

    public WebElement newAccountEmailField() {
        return findElementById(NEW_ACCOUNT_EMAIL_FIELD_ID);
    }

    public WebElement createAnAccountButton() {
        return findElementByName(CREATE_AN_ACCOUNT_BUTTON_NAME);
    }

    public WebElement accountCreationErrorMessage() {
        return findElementById(ACCOUNT_CREATION_ERROR_MESSAGE_ID);
    }
}
