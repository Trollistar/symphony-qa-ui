package is.symphony.qa.pages;

import is.symphony.qa.browser.CustomWebElement;
import org.openqa.selenium.WebElement;

public class CreateAnAccountPage extends BasePage {
    private static final String MR_RADIO_BUTTON_ID = "id_gender1";
    private static final String FIRST_NAME_FIELD_ID = "customer_firstname";
    private static final String LAST_NAME_FIELD_ID = "customer_lastname";
    private static final String PASSWORD_FIELD_ID = "passwd";
    private static final String DATE_OF_BIRTH_DAY_DROPDOWN_ID = "days";
    private static final String DATE_OF_BIRTH_MONTH_DROPDOWN_ID = "months";
    private static final String DATE_OF_BIRTH_YEAR_DROPDOWN_ID = "years";
    private static final String ADDRESS_FIRST_NAME_FIELD = "firstname";
    private static final String ADDRESS_LAST_NAME_FIELD = "lastname";
    private static final String COMPANY_FIELD_ID = "company";
    private static final String ADDRESS_FIELD_ID = "address1";
    private static final String CITY_FIELD_ID = "city";
    private static final String STATE_FIELD_ID = "id_state";
    private static final String ZIP_CODE_FIELD_ID = "postcode";
    private static final String COUNTRY_FIELD_ID = "id_country";
    private static final String ADDITIONAL_INFORMATION_FIELD_ID = "other";
    private static final String HOME_PHONE_FIELD_ID = "phone";
    private static final String MOBILE_PHONE_FIELD_ID = "phone_mobile";
    private static final String ADDRESS_ALIAS_FIELD_ID = "alias";
    private static final String REGISTER_BUTTON_ID = "submitAccount";

    public WebElement mrRadioButton() {
        return findElementById(MR_RADIO_BUTTON_ID);
    }

    public WebElement firstNameField() {
        return findElementById(FIRST_NAME_FIELD_ID);
    }

    public WebElement lastNameField() {
        return findElementById(LAST_NAME_FIELD_ID);
    }

    public WebElement passwordField() {
        return findElementById(PASSWORD_FIELD_ID);
    }

    public CustomWebElement dateOfBirthDayDropdown() {
        return findHiddenElementById(DATE_OF_BIRTH_DAY_DROPDOWN_ID);
    }

    public CustomWebElement dateOfBirthMonthDropdown() {
        return findHiddenElementById(DATE_OF_BIRTH_MONTH_DROPDOWN_ID);
    }

    public CustomWebElement dateOfBirthYearDropdown() {
        return findHiddenElementById(DATE_OF_BIRTH_YEAR_DROPDOWN_ID);
    }

    public WebElement addressFirstNameField() {
        return findElementById(ADDRESS_FIRST_NAME_FIELD);
    }

    public WebElement addressLastNameField() {
        return findElementById(ADDRESS_LAST_NAME_FIELD);
    }

    public WebElement companyField() {
        return findElementById(COMPANY_FIELD_ID);
    }

    public WebElement addressField() {
        return findElementById(ADDRESS_FIELD_ID);
    }

    public WebElement cityField() {
        return findElementById(CITY_FIELD_ID);
    }

    public CustomWebElement stateDropdown() {
        return findHiddenElementById(STATE_FIELD_ID);
    }

    public WebElement zipCodeField() {
        return findElementById(ZIP_CODE_FIELD_ID);
    }

    public CustomWebElement countryDropdown() {
        return findHiddenElementById(COUNTRY_FIELD_ID);
    }

    public WebElement additionalInformationField() {
        return findElementById(ADDITIONAL_INFORMATION_FIELD_ID);
    }

    public WebElement homePhoneField() {
        return findElementById(HOME_PHONE_FIELD_ID);
    }

    public WebElement mobilePhoneField() {
        return findElementById(MOBILE_PHONE_FIELD_ID);
    }

    public WebElement addressAliasField() {
        return findElementById(ADDRESS_ALIAS_FIELD_ID);
    }

    public WebElement registerButton() {
        return findElementById(REGISTER_BUTTON_ID);
    }
}
