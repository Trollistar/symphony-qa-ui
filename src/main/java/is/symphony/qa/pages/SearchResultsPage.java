package is.symphony.qa.pages;

import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class SearchResultsPage extends BasePage {

    private static final String PRODUCT_IMAGE_CSS = ".product_img_link";
    private static final String PRODUCT_LIST_CSS = ".product_list";

    public List<WebElement> availableProductImages() {
        return findElementsByCss(PRODUCT_IMAGE_CSS).stream().filter(WebElement::isDisplayed).collect(Collectors.toList());
    }

    public WebElement productList() {
        return findElementByCss(PRODUCT_LIST_CSS);
    }
}
