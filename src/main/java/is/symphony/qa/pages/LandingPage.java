package is.symphony.qa.pages;

import org.openqa.selenium.WebElement;

public class LandingPage extends BasePage {

    private static final String LOGO_CSS = ".logo";
    private static final String CATEGORY_BASE_XPATH = "//a[text()='%s']";
    private static final String SEARCH_FIELD_ID = "search_query_top";
    private static final String SEARCH_BUTTON_NAME = "submit_search";

    public WebElement logo() {
        return findElementByCss(LOGO_CSS);
    }

    public WebElement category(String categoryName) {
        return findElementByXpath(String.format(CATEGORY_BASE_XPATH, categoryName));
    }

    public WebElement searchField() {
        return findElementById(SEARCH_FIELD_ID);
    }

    public WebElement searchButton() {
        return findElementByName(SEARCH_BUTTON_NAME);
    }
}
