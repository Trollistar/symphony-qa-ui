package is.symphony.qa.pages;

import is.symphony.qa.browser.CustomWebElement;
import is.symphony.qa.browser.DriverManager;
import is.symphony.qa.helpers.LoggingHelper;
import is.symphony.qa.helpers.TestConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage {

    /**
     * Main method for locating elements. Easily extendable to suit QA's needs.
     *
     * @param expectedCondition Condition used to discern whether the element is ready for use.
     * @return Either a desired element or null with the appropriate message.
     */
    private static CustomWebElement findElement(ExpectedCondition<WebElement> expectedCondition) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TestConfiguration.getTimeout());
            WebElement element = wait.until(expectedCondition);
            return new CustomWebElement(element);
        } catch (TimeoutException exception) {
            LoggingHelper.log("Could not locate an element with locator: " + expectedCondition.toString());
            return null;
        }
    }

    /**
     * Locates an element that must be visible in the DOM before returning it.
     *
     * @param by locator that is going to be used to retrieve the element we want to interact with.
     * @return either a desired element or null with the appropriate message.
     */
    private static CustomWebElement findVisibleElement(By by) {
        return findElement(ExpectedConditions.visibilityOfElementLocated(by));
    }

    /**
     * Locates an element that is not necessarily actually displayed in the DOM.
     * The only requirement it has to fulfill is to not be null.
     *
     * @param by locator that is going to be used to retrieve the element we want to interact with.
     * @return either a desired element or null with the appropriate message.
     */
    private static CustomWebElement findHiddenElement(By by) {
        return findElement(ExpectedConditions.presenceOfElementLocated(by));
    }

    /**
     * Retrieves all the elements matching the provided locator.
     *
     * @param by locator that is going to be used to retrieve the elements.
     * @return a list with all the elements matching the provided locator.
     */
    private static List<WebElement> findElements(By by) {
        return DriverManager.getDriver().findElements(by);
    }

    CustomWebElement findElementByXpath(String xpath) {
        return findVisibleElement(By.xpath(xpath));
    }

    CustomWebElement findElementByName(String name) {
        return findVisibleElement(By.xpath(String.format("//*[@name='%s']", name)));
    }

    CustomWebElement findElementByCss(String cssSelector) {
        return findVisibleElement(By.cssSelector(cssSelector));
    }

    CustomWebElement findElementById(String id) {
        return findVisibleElement(By.id(id));
    }

    CustomWebElement findHiddenElementById(String id) {
        return findHiddenElement(By.id(id));
    }

    List<WebElement> findElementsByCss(String cssSelector) {
        return findElements(By.cssSelector(cssSelector));
    }
}

