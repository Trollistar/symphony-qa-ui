package is.symphony.qa.pages;

public class AutomationPractice {
    private LandingPage landingPage;
    private HeaderPage headerPage;
    private AuthenticationPage authenticationPage;
    private CreateAnAccountPage createAnAccountPage;
    private SearchResultsPage searchResultsPage;

    public AutomationPractice() {
        landingPage = new LandingPage();
        headerPage = new HeaderPage();
        authenticationPage = new AuthenticationPage();
        createAnAccountPage = new CreateAnAccountPage();
        searchResultsPage = new SearchResultsPage();
    }

    public LandingPage landingPage() {
        return landingPage;
    }

    public HeaderPage headerPage() {
        return headerPage;
    }

    public AuthenticationPage authenticationPage() {
        return authenticationPage;
    }

    public CreateAnAccountPage createAnAccountPage() {
        return createAnAccountPage;
    }

    public SearchResultsPage searchResultsPage() {
        return searchResultsPage;
    }
}
