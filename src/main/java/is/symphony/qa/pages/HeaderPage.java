package is.symphony.qa.pages;

import org.openqa.selenium.WebElement;

public class HeaderPage extends BasePage {
    private static final String SIGN_IN_BUTTON_CSS = "a.login";
    private static final String SIGN_OUT_BUTTON_CSS = "a.logout";

    public WebElement signInButton() {
        return findElementByCss(SIGN_IN_BUTTON_CSS);
    }

    public WebElement signOutButton() {
        return findElementByCss(SIGN_OUT_BUTTON_CSS);
    }
}
