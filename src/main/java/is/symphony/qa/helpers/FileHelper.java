package is.symphony.qa.helpers;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class FileHelper {

    public static void writeListToFile(List<String> list) {
        try {
            FileUtils.writeLines(new File(TestConfiguration.getProductsFileName()), list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> readTextFromFile() {
        try {
            return FileUtils.readLines(new File(TestConfiguration.getProductsFileName()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
