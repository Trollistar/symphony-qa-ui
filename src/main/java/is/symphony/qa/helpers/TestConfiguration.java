package is.symphony.qa.helpers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestConfiguration {

    private static Properties appProperties;

    private TestConfiguration() {
    }

    public static int getTimeout() {
        String timeout = getProperty("timeout");
        if (timeout == null) {
            return 5;
        }
        return Integer.parseInt(timeout);
    }

    public static String getBrowserType() {
        return getProperty("browser");
    }

    public static String getEnvironment() {
        return getProperty("environment");
    }

    public static String getProductsFileName() {
        return getProperty("productsFileName");
    }

    /**
     * Used to retrieve the various configurations from the app.properties file.
     * Simply pass in the key of the desired config row and you will get its value back.
     *
     * @param key which key the value will be returned from
     * @return return the value of the passed key in the form of a String
     */
    private static String getProperty(String key) {
        if (appProperties == null) {
            FileReader reader = null;

            try {
                reader = new FileReader("app.properties");
            } catch (FileNotFoundException e) {
                LoggingHelper.log("File app.properties not found in the project root.");
            }

            if (reader == null) {
                LoggingHelper.log("Could not instantiate reader within PropertiesHelper.getProperty().");
                return null;
            }

            appProperties = new Properties();

            try {
                appProperties.load(reader);
            } catch (IOException e) {
                LoggingHelper.log("Could not read from app.properties file.");
            }
        }

        return appProperties.getProperty(key);
    }
}
