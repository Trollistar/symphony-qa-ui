package is.symphony.qa.helpers;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Small logging helper for more readable console outputs.
 */
public class LoggingHelper {

    private static final String LOGGER_NAME = "com.hyperoptic.qa.logger";
    private static Logger logger;

    private LoggingHelper() {
    }

    public static void log(String message) {
        if (logger == null) {
            logger = Logger.getLogger(LOGGER_NAME);
            ConsoleHandler handler = new ConsoleHandler();
            handler.setLevel(Level.ALL);
            logger.addHandler(handler);
            logger.setLevel(Level.ALL);
        }

        logger.log(Level.ALL, message);
    }
}