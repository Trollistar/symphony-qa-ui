# Symphony UI test automation task

---

## Introduction

This framework was created as a part of Symphony job interview.

The following requirements were presented:

* Introduce test automation framework - **Completed**
* Add automated tests listed in the shared pdf file - **Completed**

The following nice-to-haves were presented:

* Framework should be based on Page Object Model - **Completed**
* Add negative test scenarios - **Completed**

---

## Prerequisites

* **Java 13 JDK** - As there were no specific limitations when it comes to Java version, the second-to-latest one was used.
* **Maven** - This project relies on Maven for package management and project building.

If IntelliJ Idea is your IDE of choice, there are several requirements related to it:

* **Cucumber for Java plugin** - [LINK](https://plugins.jetbrains.com/plugin/7212-cucumber-for-java)
* **Gherkin plugin** - [LINK](https://plugins.jetbrains.com/plugin/9164-gherkin)

---

## Running the tests

To execute the test suite, run the install maven goal.

```console
mvn install
```

---

## Test reports

Generated surefire html reports can be accessed under root\target\surefire-reports\index.html.

---

## Short overview of the framework structure

The key features of the framework will be listed below in no particular order.

### WebDriver management

WebDriver management is a joint effort of two classes.

**DriverFactory** takes care of versioning for driver binaries based on currently installed browser versions and instantiating proper drivers to utilize during the tests.

**DriverManager** ensures that driver instances are properly distributed across threads accessing it, allowing for effective test paralellization on scenario level.

### Element management

This framework relies on **Page Object Model** to keep the dom elements we want to interact with nicely encapsulated and maintainable. This is achieved through the individual **Page** classes and the important **BasePage** class that all of them inherit from.

**BasePage** serves to centralize the logic for retrieving the elements from the dom in a predictable, configurable and easily extensible way.

### Test features

The framework relies on Gherkin syntax and Cucumber to connect the business world with the development world, providing easily readable test cases and providing an actions layer for our tests.

---

## Built with

* [Selenium](https://www.seleniumhq.org/) - The best library for browser automation
* [WebDriverMananger](https://github.com/bonigarcia/webdrivermanager) - Fantastic library for WebDriver management
* [Cucumber](https://cucumber.io/) - Library that allows writing tests as BDD style user stories
* [TestNG](https://testng.org/doc/) - Excellent test runner for Java
* [Faker](https://github.com/DiUS/java-faker) - Large library for test data generation
---

## Author

**Slobodan Popović** - slobapop90@gmail.com

---
